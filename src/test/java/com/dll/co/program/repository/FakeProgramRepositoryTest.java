package com.dll.co.program.repository;

import com.dll.co.program.domain.FakeProgram;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;
@RunWith(SpringRunner.class)
@SpringBootTest
public class FakeProgramRepositoryTest {

    @Autowired
    private FakeProgramRepository fakeProgramRepository;

    @Before
    public void setUp() throws Exception {
        FakeProgram fakeProgram = new FakeProgram("My Test Program", 0.0, null);

        //save user, verify has ID value after save
        assertNull(fakeProgram.getId());

        this.fakeProgramRepository.save(fakeProgram);
        assertNotNull(fakeProgram.getId());

    }

    @After
    public void tearDown() throws Exception {
        FakeProgram fakeProgram = fakeProgramRepository.findByName("My Test Program");
        this.fakeProgramRepository.delete(fakeProgram);
    }

    @Test
    public void findByName() {
        FakeProgram fakeProgram = fakeProgramRepository.findByName("My Test Program");
        assertNotNull(fakeProgram);
        assertEquals(0.0, fakeProgram.getAmount().doubleValue(), 0);

    }
}