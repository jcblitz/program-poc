package com.dll.co.program.controller;

import com.dll.co.program.domain.FakeProgram;
import com.dll.co.program.repository.FakeProgramRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@Api(value="Program", description="CRUD operations for programs")
public class ProgramController {

    @Autowired
    private FakeProgramRepository fakeProgramRepository;

    @GetMapping(value = "/programs", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "List of all programs",
            notes = "Status of the Program does not matter, they are all returned", response = Iterable.class)
    public Iterable<FakeProgram> programs() {
        return fakeProgramRepository.findAll();
    }

    @GetMapping(value = "/programs/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Retrieve program by id", response = FakeProgram.class)
    public FakeProgram program(@PathVariable Integer id) throws Exception {
        Optional<FakeProgram> fakeProgram = fakeProgramRepository.findById(id);

        if (!fakeProgram.isPresent())
            throw new Exception("id-" + id);

        return fakeProgram.get();
    }
}