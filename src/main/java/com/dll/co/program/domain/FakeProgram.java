package com.dll.co.program.domain;

import io.swagger.annotations.ApiModelProperty;

import javax.persistence.*;

/*
CREATE TABLE FakePrograms(
   id     INTEGER  NOT NULL PRIMARY KEY
  ,name   VARCHAR(29) NOT NULL
  ,amount NUMERIC(6,2) NOT NULL
  ,Remark VARCHAR(45)
);
 */

@Entity
@Table(name = "FakePrograms")
public class FakeProgram {
    @Id
    @GeneratedValue(strategy = javax.persistence.GenerationType.IDENTITY)
    private Integer id;
    @ApiModelProperty(notes = "The name of the program. This must be unique", required = true)
    private String name;
    @ApiModelProperty(notes = "Amount of discount applied to the program", dataType = "double")
    private Double amount;
    @ApiModelProperty(notes = "Any additional notes about the program")
    private String remark;

    public FakeProgram() {
    }

    public FakeProgram(String name, Double amount, String remark) {
        this.name = name;
        this.amount = amount;
        this.remark = remark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
