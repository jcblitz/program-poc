package com.dll.co.program.repository;

import com.dll.co.program.domain.FakeProgram;
import org.springframework.data.repository.CrudRepository;

public interface FakeProgramRepository extends CrudRepository<FakeProgram, Integer> {
    FakeProgram findByName(String name);
}

