CREATE TABLE fake_programs(
   id     INTEGER NOT NULL IDENTITY(1,1) PRIMARY KEY
  ,name   VARCHAR(255) NOT NULL
  ,amount NUMERIC(6,2) NOT NULL
  ,Remark VARCHAR(500)
  ,
  CONSTRAINT UC_name UNIQUE (name)
);
INSERT INTO fake_programs(name,amount,Remark) VALUES ('Insiders Program',345.33,'Pays on time');
INSERT INTO fake_programs(name,amount,Remark) VALUES ('Awesome Customer Program',993.44,NULL);
INSERT INTO fake_programs(name,amount,Remark) VALUES ('The Third Program',0,'Great to work with
and always pays with cash.');
INSERT INTO fake_programs(name,amount,Remark) VALUES ('Last but not least program',2344,NULL);
