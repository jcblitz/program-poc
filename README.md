# Program

Welcome to the Program POC

## Purpose

To stand up a basic Spring Boot REST API that can serve the Program from a resource perspective. 

`GET /programs`

Return all programs

`GET /programs/id`

Return a specific program by ID with all associated data

## Configuration

There are two components, the Spring Boot web application and the Sql Server database.

Flyway is used to manage the database schema.

The `pom.xml` file contains configurations for the database. Currently it's configured to my Azure Sql instance with the **password passed in as an environment variable** (ask me for it). Or, create your own and update the `pom.xml` file accordingly. 

`application.properties` will be parsed with values from `pom.xml` and the Flyway configuration in `pom.xml` will reference the property values as well. Note the driver is hard coded as changing the driver would have bigger implications (e.g., the flyway scripts won't work)

## Useful Commands

`mvn flyway:migrate`

Provisions the database

`mvn package`

Builds the executable jar. Configured to output as `ROOT.jar` for Azure's default configuration

`mvn spring-boot:run`

Run the Spring Boot application

 